#include <SoftwareSerial.h>

#define PIN_STBY  2
#define PIN_PWMA  3
#define PIN_AIN1  4
#define PIN_AIN2  5
#define PIN_PWMB  9
#define PIN_BIN1  6
#define PIN_BIN2  7

#define PIN_S_TRIG  8
#define PIN_S_ECHO  12

#define PIN_BT_RX  11
#define PIN_BT_TX  10

SoftwareSerial softSerial(PIN_BT_TX, PIN_BT_RX);
char serialData = 0;

void brake() {
  // both motors brake
  digitalWrite(PIN_AIN1, HIGH);
  digitalWrite(PIN_AIN2, HIGH);
  digitalWrite(PIN_BIN1, HIGH);
  digitalWrite(PIN_BIN2, HIGH);
}

void stop() {
  digitalWrite(PIN_AIN1, LOW);
  digitalWrite(PIN_AIN2, LOW);
  digitalWrite(PIN_BIN1, LOW);
  digitalWrite(PIN_BIN2, LOW);

  digitalWrite(PIN_PWMA, LOW);
  digitalWrite(PIN_PWMB, LOW);

 // enable standby
  digitalWrite(PIN_STBY, LOW);
}

void turn(int right) {
    switch(right) {
      default:
      case 0:
        digitalWrite(PIN_PWMA, HIGH);
        digitalWrite(PIN_PWMB, HIGH);
        break;
      case 1:
        digitalWrite(PIN_PWMA, HIGH);
        digitalWrite(PIN_PWMB, LOW);
        break;
      case -1:
        digitalWrite(PIN_PWMA, LOW);
        digitalWrite(PIN_PWMB, HIGH);
        break;
    }
}

void move(int speed, int direction) {
  // disable standby
  digitalWrite(PIN_STBY, HIGH);
  
  if (speed > 0) {
    // both motors clockwise
    digitalWrite(PIN_AIN1, HIGH);
    digitalWrite(PIN_AIN2, LOW);
    digitalWrite(PIN_BIN1, HIGH);
    digitalWrite(PIN_BIN2, LOW);

    turn(direction);
  } else if (speed < 0) {
    // both motors counter-clockwise
    digitalWrite(PIN_AIN1, LOW);
    digitalWrite(PIN_AIN2, HIGH);
    digitalWrite(PIN_BIN1, LOW);
    digitalWrite(PIN_BIN2, HIGH);
    
    turn(direction);
  } else {
    brake();
    delay(100);
    stop();
  }
}

unsigned int distance() {
  unsigned int duration = 0;
  
  digitalWrite(PIN_S_TRIG, LOW);
  delayMicroseconds(2);
  digitalWrite(PIN_S_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_S_TRIG, LOW);

  duration = pulseIn(PIN_S_ECHO, HIGH);
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return duration / 29 / 2;
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  
  // Motor controller setup.
  pinMode(PIN_STBY, OUTPUT); // STBY
  pinMode(PIN_PWMA, OUTPUT); // PWMA
  pinMode(PIN_AIN1, OUTPUT); // AIN1
  pinMode(PIN_AIN2, OUTPUT); // AIN2
  pinMode(PIN_PWMB, OUTPUT); // PWMB
  pinMode(PIN_BIN1, OUTPUT); // BIN1
  pinMode(PIN_BIN2, OUTPUT); // BIN2
  stop();

  // Sonar setup.
  pinMode(PIN_S_TRIG, OUTPUT);
  pinMode(PIN_S_ECHO, INPUT);

  //Serial.begin(9600);

  // Serial connection to Bluetooth.
  softSerial.begin(9600);
}

void test_motors() {
  static int count = 0;
  
  if (count < 4) {
    switch(count) {
      default:
      case 0:
        move(1, 0);
        break;
      case 1:
        move(1, 1);
        break;
      case 2:
        move(-1, -1);
        break;
      case 3:
        move(-1, 0);
        break;
    }
    count++;
  } else {
    stop();
  }

  delay(1000);
}

void test_distance() {
  unsigned int d;

  d = distance();

  if (d <= 10)
    digitalWrite(LED_BUILTIN, HIGH);
  else
    digitalWrite(LED_BUILTIN, LOW);

  Serial.print(d);
  Serial.print(" cm\r\n");
  delay(500);
}

void loop() {
  // Distance measurements take too much time
  // and cause issues with bluetooth control.
/*  const unsigned int min_d = 20;
  unsigned int d;

  d = distance();

  if (d <= min_d)
    digitalWrite(LED_BUILTIN, HIGH);
  else
    digitalWrite(LED_BUILTIN, LOW);*/
 
  // Control via smartphone, over bluetooth (HC-05).
  if (softSerial.available() > 0) {
    serialData = softSerial.read();
    if (serialData == 'F') {        // Move forward
      move(1, 0);
    } else if (serialData == 'B') { // Move backwards
      move(-1, 0);
    } else if (serialData == 'R') { // Turn right
      move(1, 1);
    } else if (serialData == 'L') { // Turn left
      move(1, -1);
    } else {
      stop();
    }
  }
}
